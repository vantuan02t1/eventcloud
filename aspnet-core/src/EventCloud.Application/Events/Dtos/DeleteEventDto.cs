﻿using System;
using System.ComponentModel.DataAnnotations;

namespace EventCloud.Events.Dtos
{
    public class DeleteEventDto
    {
        public string Id { get; set; }
    }
}
