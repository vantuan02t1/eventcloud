﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EventCloud.Migrations
{
    public partial class DeleteTenantIdFromEvent : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "AppEvents");

            migrationBuilder.DropColumn(
                name: "TenantId",
                table: "AppEventRegistrations");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "AppEvents",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TenantId",
                table: "AppEventRegistrations",
                nullable: false,
                defaultValue: 0);
        }
    }
}
