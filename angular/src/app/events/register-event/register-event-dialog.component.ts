import { Component, Injector, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { finalize } from 'rxjs/operators';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  EventServiceProxy,
  UserServiceProxy,
  PagedResultDtoOfUserDto,
  EventListDto,
  EntityDtoOfGuid,
  EventRegisterOutput,
  CreateEventInput,
  UserDto
} from '@shared/service-proxies/service-proxies';

@Component({
  templateUrl: 'register-event-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
    `
  ]
})
export class RegisterEventDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  event: EventListDto = new EventListDto();
  users: UserDto[] = [];

  constructor(
    injector: Injector,
    private _eventService: EventServiceProxy,
    private _userService: UserServiceProxy,
    private _dialogRef: MatDialogRef<RegisterEventDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _id: string
  ) {
    super(injector);
  }

  ngOnInit(): void {
    if(this._id != undefined && this._id != null){
      this._eventService.getDetailAsync(this._id)
      .subscribe((result: EventListDto) => {
          this.event = result;
      });

      // this._userService
      //       .getAll('', true, 0, 1000)
      //       .pipe(
      //           finalize(() => {
      //           })
      //       )
      //       .subscribe((result: PagedResultDtoOfUserDto) => {
      //           this.users = result.items;
      //           console.log(this.users);
      //       });
    }
  }

  registerToEvent(): void {
    var input = new EntityDtoOfGuid();
    input.id = this.event.id;

    this._eventService.registerAsync(input)
        .subscribe((result: EventRegisterOutput) => {
            abp.notify.success('Successfully registered to event. Your registration id: ' + result.registrationId + ".");
            this.close(true);
        });
  };

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
