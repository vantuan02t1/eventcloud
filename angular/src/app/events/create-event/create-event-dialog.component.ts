import { Component, Injector, Inject, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {FormControl} from '@angular/forms';
import { finalize } from 'rxjs/operators';
import * as moment from 'moment';
import * as _ from 'lodash';
import { AppComponentBase } from '@shared/app-component-base';
import {
  EventServiceProxy,
  EventListDto,
  CreateEventInput
} from '@shared/service-proxies/service-proxies';
import { getDate } from 'ngx-bootstrap/chronos/utils/date-getters';

@Component({
  templateUrl: 'create-event-dialog.component.html',
  styles: [
    `
      mat-form-field {
        width: 100%;
      }
      mat-checkbox {
        padding-bottom: 5px;
      }
    `
  ]
})
export class CreateEventDialogComponent extends AppComponentBase
  implements OnInit {
  saving = false;
  event: EventListDto = new EventListDto();
  expirationDate = <any>undefined;
  date = new FormControl(new Date());
  titleEvent = 'Create New Event';

  constructor(
    injector: Injector,
    private _eventService: EventServiceProxy,
    private _dialogRef: MatDialogRef<CreateEventDialogComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _id: string
  ) {
    super(injector);
  }

  ngOnInit(): void {
    if(this._id != undefined && this._id != null){
      this._eventService.getDetailAsync(this._id)
    .subscribe((result: EventListDto) => {
        this.event = result;
        this.titleEvent = 'Edit Event';
        this.date = new FormControl(moment(result.date).format("YYYY-MM-DD").toString());
    });
    }
  }


  save(): void {
    this.saving = true;
    const event_ = new CreateEventInput();
    event_.init(this.event);
    event_.id = this.event.id;

    var expirationDate = moment(this.date.value).add(1, 'day');
    event_.date = expirationDate;

    this._eventService
      .createAsync(event_)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
