import { Component, Injector, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { finalize } from 'rxjs/operators';
import { appModuleAnimation } from '@shared/animations/routerTransition';
import { EventServiceProxy, DeleteEventDto, EventListDto, ListResultDtoOfEventListDto, EntityDtoOfGuid } from '@shared/service-proxies/service-proxies';
import { PagedListingComponentBase, PagedRequestDto } from 'shared/paged-listing-component-base';
import { CreateEventDialogComponent } from 'app/events/create-event/create-event-dialog.component';
import { RegisterEventDialogComponent } from 'app/events/register-event/register-event-dialog.component';
import { AppSessionService } from '@shared/session/app-session.service';

class PagedEventsRequestDto extends PagedRequestDto {
    keyword: string;
}

@Component({
    templateUrl: './events.component.html',
    animations: [appModuleAnimation()]
})
export class EventsComponent extends PagedListingComponentBase<EventListDto> {

    keyword = '';

    active: boolean = false;
    events: EventListDto[] = [];
    includeCanceledEvents:boolean=false;
    errorMessage = '';

    constructor(
        injector: Injector,
        private _eventService: EventServiceProxy,
        private _dialog: MatDialog,
        private _sessionService: AppSessionService
    ) {
        super(injector);
    }

    list(
        request: PagedEventsRequestDto,
        pageNumber: number,
        finishedCallback: Function
    ): void {

        request.keyword = this.keyword;

        this._eventService
            .getListAsync(true)
            .pipe(
                finalize(() => {
                    finishedCallback();
                })
            )
            .subscribe((result: ListResultDtoOfEventListDto) => {
                this.events = result.items;
            });
    }

    createEvent(): void {
        this.showCreateOrEditEventDialog();
    }

    editEvent(event: EventListDto): void {
        if(this._sessionService.user['id'] == event.creatorUserId){
            this.showCreateOrEditEventDialog(event.id);
        }
        else{
            this.errorMessage = "You cannot edit event: " + event.title;
        }
    }

    registerEvent(event: EventListDto): void{
        this.showRegisterEventDialog(event.id);
    }

    cancelEvent(event: EventListDto): void{
        if(this._sessionService.user['id'] == event.creatorUserId){
            abp.message.confirm(
                this.l('Cancel this event: ' + event.title, ""),
                (result: boolean) => {
                    if (result) {
                        var input = new EntityDtoOfGuid();
                        input.id = event.id;
                        this._eventService.cancelAsync(input).pipe(
                            finalize(() => {
                                abp.notify.success(this.l('Canceled event successful!'));
                                this.refresh();
                            })
                        )
                        .subscribe(() => { });
                    }
                }
            );
        }
        else{
            this.errorMessage = "You cannot cancel event: " + event.title;
        }        
    }

    deleteEvent(event: EventListDto): void{
        var input = new DeleteEventDto();
        input.id = event.id;
        if(this._sessionService.user['id'] == event.creatorUserId){
            abp.message.confirm(
                this.l('Delete this event: ' + event.title, ""),
                (result: boolean) => {
                    if (result) {
                        this._eventService.deleteAsync(input.toJSON()['id']).pipe(
                            finalize(() => {
                                abp.notify.success(this.l('SuccessfullyDeleted'));
                                this.refresh();
                            })
                        )
                        .subscribe(() => { });
                    }
                }
            );
        }
        else{
            this.errorMessage = "You cannot delete event: " + event.title;
        }       
    }

    showCreateOrEditEventDialog(id?: string): void {
        let createOrEditRoleDialog;
        if (id === undefined || id == null) {
            createOrEditRoleDialog = this._dialog.open(CreateEventDialogComponent);
        }else {
            createOrEditRoleDialog = this._dialog.open(CreateEventDialogComponent, {
                data: id
            });
        }

        createOrEditRoleDialog.afterClosed().subscribe(result => {
            if (result) {
                this.refresh();
            }
        });
    }

    showRegisterEventDialog(id?: string): void {
        let registerEventDialog;
        if (id !== undefined || id != null) {
            registerEventDialog = this._dialog.open(RegisterEventDialogComponent, {
                data: id
            });
        }

        registerEventDialog.afterClosed().subscribe(result => {
            if (result) {
                this.refresh();
            }
        });
    }

    protected delete(event: EntityDtoOfGuid): void {
        // abp.message.confirm(
        //     'Are you sure you want to cancel this event?',
        //     (result: boolean) => {
        //         if (result) {
        //             this._eventService.cancelAsync(event)
        //                 .subscribe(() => {
        //                     abp.notify.info('Event is deleted');
        //                     this.refresh();
        //                 });
        //         }
        //     }
        // );
    }

    loadEvent() {
        // this._eventService.getListAsync(this.includeCanceledEvents)
        //     .subscribe((result: ListResultDtoOfEventListDto) => {
        //         this.events = result.items;
        //     });
    }
}
